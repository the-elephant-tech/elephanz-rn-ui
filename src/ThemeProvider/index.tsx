import React, {
  ReactElement, useState,
} from 'react';
import ThemeContext from '../Theme/ThemeContext';
import DefaultTheme from '../Theme/DefaultTheme';
import {
  ThemeColors,
} from '../Theme/types';

interface Props {
  Theme?: ThemeColors;
  children: ReactElement;
}

const ThemeProvider = (props: Props) => {
  const {
    Theme,
    children,
  } = props;
  const [theme, setTheme] = useState<ThemeColors>(Theme || DefaultTheme);
  return (
    <ThemeContext.Provider
      value={{
        theme,
        setTheme: (currentTheme: ThemeColors) => setTheme(currentTheme),
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
};
export default ThemeProvider;
