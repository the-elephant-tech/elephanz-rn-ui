import React, {
  useState,
} from 'react';
import {
  Image, ImageProps, ImageSourcePropType,
} from 'react-native';

interface Props extends ImageProps {
  fallbackImage: ImageSourcePropType;
}

interface State {
  hasError: boolean;
}

export const ExtendedImage: React.FC<
Props
> = (props) => {
  const [hasError] = useState(0);

  const {
    fallbackImage,
    ...imageProps
  } = props;

  if (!hasError) {
    return (
      <Image
        {...imageProps}
      />
    );
  }
  return (
    <Image
      {...imageProps}
      source={fallbackImage}
    />
  );
};

export default ExtendedImage;
