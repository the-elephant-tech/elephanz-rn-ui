import {
  ImageStyle, StyleSheet, TextStyle, ViewStyle,
} from 'react-native';

interface StylesInterface {
  activeContainer: ViewStyle;
  signinButtonActiveText: TextStyle;
  inActiveContainer: ViewStyle;
  signinButtonInactiveText: TextStyle;
  buttonIcon: ImageStyle;
}

export const btnStyles = StyleSheet.create<StylesInterface>({
  activeContainer: {
    flexDirection: 'row',
    borderRadius: 25,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signinButtonActiveText: {
    // color: theme.primaryColor,
    fontSize: 15,
  },
  inActiveContainer: {
    // backgroundColor: theme.primaryColor,
    borderRadius: 25,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signinButtonInactiveText: {
    // color: theme.secondaryColor,
    fontSize: 15,
  },
  buttonIcon: {
    width: 24,
    height: 28,
    marginLeft: 27,
    marginRight: 17,
  },
});

export default btnStyles;
