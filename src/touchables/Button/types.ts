import {
  ImageSourcePropType, ImageStyle, TextStyle, ViewStyle,
} from 'react-native';

export interface ButtonProps {
  isLoading?: boolean;
  activeStyle?: ViewStyle;
  inactiveStyle?: ViewStyle;
  text?: string;
  disabled: boolean;
  onPress: () => void;
  buttonIcon?: ImageSourcePropType;
  textStyles?: TextStyle;
  buttonIconStyles?: ImageStyle;
  spinnerColor?: string;
  spinnerSize?: number | 'small' | 'large';
}
