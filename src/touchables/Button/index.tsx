import React, {
  useContext,
} from 'react';
import {
  ActivityIndicator, Image, Text, TouchableOpacity,
} from 'react-native';
import Theme from '../../Theme/DefaultTheme';
import ThemeContext from '../../Theme/ThemeContext';
import {
  btnStyles,
} from './styles';
import {
  ButtonProps,
} from './types';

export const ButtonComponent: React.FC<
ButtonProps
> = (props) => {
  const {
    theme,
  } = useContext(ThemeContext);
  const {
    isLoading,
    activeStyle,
    inactiveStyle,
    text,
    disabled,
    onPress,
    buttonIcon,
    textStyles,
    buttonIconStyles,
    spinnerColor,
    spinnerSize,
  } = props;
  return (
    <TouchableOpacity
      style={
        disabled
          ? [btnStyles.inActiveContainer, inactiveStyle]
          : [
            btnStyles.activeContainer,
            {
              backgroundColor: Theme.primaryColor,
            },
            {
              backgroundColor: theme.primaryColor,
            },
            activeStyle,
          ]
      }
      disabled={disabled}
      onPress={onPress}
    >
      {!isLoading ? (
        <>
          {buttonIcon && (
            <Image
              style={[btnStyles.buttonIcon, buttonIconStyles]}
              source={buttonIcon}
            />
          )}
          <Text style={[btnStyles.signinButtonInactiveText, textStyles]}>
            {text}
          </Text>
        </>
      ) : (
        <ActivityIndicator size={spinnerSize} color={spinnerColor} />
      )}
    </TouchableOpacity>
  );
};
export default ButtonComponent;
