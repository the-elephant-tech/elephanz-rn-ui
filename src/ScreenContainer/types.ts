import {
  ViewStyle,
} from 'react-native';

export interface Props {
  headerRender?: React.ReactNode;
  bottomCard?: BottomCardProps;
  contentContainerStyle?: ViewStyle;
  keyboardExtraHeight?: number;
}

export interface BottomCardProps {
  render: React.ReactNode;
  height: number;
}
