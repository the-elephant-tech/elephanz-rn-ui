import {
  StyleSheet, ViewStyle,
} from 'react-native';

interface Styles {
  screenContainer: ViewStyle;
  bodyContainer: ViewStyle;
  scrollView: ViewStyle;
  scrollViewContentContainer: ViewStyle;
  bottomCardContainer: ViewStyle;
}

export const styles = StyleSheet.create<Styles>({
  screenContainer: {
    flex: 1,
  },
  bodyContainer: {
    flex: 1,
    position: 'relative',
  },
  scrollView: {
  },
  scrollViewContentContainer: {
    flexDirection: 'column',
    minHeight: '100%',
  },
  bottomCardContainer: {
    width: '100%',
    left: 0,
    bottom: 0,
    overflow: 'hidden',
    position: 'absolute',
    elevation: 4,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.23,
    shadowRadius: 0.23,
  },
});

export default styles;
