import {
  TextStyle, ViewStyle,
} from 'react-native';

export interface Props {
  title?: string;
  headerContainerStyle?: ViewStyle;
  textContainerStyle?: ViewStyle;
  textStyle?: TextStyle;
  onPress: () => void;
  left: () => JSX.Element;
  center: () => JSX.Element;
  right: () => JSX.Element;
  dimensions?: headerWithFlex | headerWithWidth;
  height: number;
}

export type headerWithFlex = {
  flex: number[];
};
export type headerWithWidth = {
  width: number[];
};

export enum Position {
  LEFT,
  CENTER,
  RIGHT,
}

export function isFlex(dimensions: headerWithWidth | headerWithFlex): dimensions is headerWithFlex {
  return !!(dimensions as headerWithFlex).flex;
}
