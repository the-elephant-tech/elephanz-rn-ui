import {
  ImageStyle, StyleSheet, TextStyle, ViewStyle,
} from 'react-native';

interface Styles {
  headerContainer: ViewStyle;
  buttonImageContainer: ViewStyle;
  buttonImage: ImageStyle;
  textContainerStyle: ViewStyle;
  textStyle: TextStyle;
}

export const styles = StyleSheet.create<Styles>({
  headerContainer: {
    height: 60,
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonImageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonImage: {
    height: 16,
    width: 28,
    resizeMode: 'contain',
  },
  textContainerStyle: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    textAlign: 'center',
  },
});

export default styles;
