import React, {
  FunctionComponent,
} from 'react';
import {
  View,
  ViewStyle,
} from 'react-native';
import {
  SvgProps,
} from 'react-native-svg';

interface Props extends SvgProps {
  containerStyle?: ViewStyle;
  svgFile: FunctionComponent<SvgProps>;
}

export const ExtendedSVG: React.FC<
Props
> = (props) => {
  const {
    containerStyle,
    svgFile,
    ...svgProps
  } = props;
  return (
    <View
      style={containerStyle}
    >
      <props.svgFile
        {...svgProps}
        width="100%"
        height="100%"
      />
    </View>
  );
};

export default ExtendedSVG;
