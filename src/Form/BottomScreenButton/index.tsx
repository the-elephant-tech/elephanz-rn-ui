import React from 'react';
import {
  View,
} from 'react-native';
import ButtonComponent from '../../touchables/Button';
import {
  styles,
} from './styles';
import {
  BottomScreenButtonProps,
} from './types';

export const BottomScreenButton: React.FC<
BottomScreenButtonProps
> = (props) => {
  const {
    isLoading,
    upperMinHeight,
    text,
    disabled,
    onPress,
    bottomMinHeight,
    bottomMaxHeight,
  } = props;
  return (
    <View
      style={styles.mainContainer}
    >
      <View
        style={{
          flex: 1,
          minHeight: upperMinHeight,
        }}
      />
      <ButtonComponent
        text={text}
        disabled={disabled}
        onPress={onPress}
        isLoading={isLoading}
      />
      <View
        style={{
          flex: 1,
          minHeight: bottomMinHeight,
          maxHeight: bottomMaxHeight,
        }}
      />
    </View>
  );
};

export default BottomScreenButton;
