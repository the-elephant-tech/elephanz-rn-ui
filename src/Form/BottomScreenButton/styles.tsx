import {
  StyleSheet, ViewStyle,
} from 'react-native';

interface StylesInterface {
  mainContainer: ViewStyle;
}

export const styles = StyleSheet.create<StylesInterface>({
  mainContainer: {
    flexGrow: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'stretch',
  },
});

export default styles;
