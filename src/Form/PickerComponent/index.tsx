import {
  Picker,
} from 'native-base';
import React from 'react';
import {
  Platform, Text, View,
} from 'react-native';
import {
  FakeEvent,
} from '../types';
import styles from './styles';
import {
  PickerItem, PickerProps, PropStylesInterface,
} from './types';

export const PickerComponent = (props: React.PropsWithChildren<PickerProps>) => {
  // const focus = () => {
  //   if (pickerRef) {
  //     // todo
  //   }
  // };

  // const blur = () => {
  //   if (pickerRef) {
  //     // console.warn('blurred');
  //   }
  // };

  const renderItems = (items: PickerItem[]) => {
    const {
      placeholder, textInactiveColor, textActiveColor,
    } = props;
    const itemsToRender = [];
    if (Platform.OS === 'android') {
      itemsToRender.push(
        <Picker.Item
          label={placeholder}
          value={null}
          key={-1}
          color={textInactiveColor}
        />,
      );
    }
    items.forEach((item) => {
      if (item && item.label && item.value) {
        itemsToRender.push(
          <Picker.Item
            label={item.label}
            value={item.value}
            key={itemsToRender.length - 1}
            color={textActiveColor}
          />,
        );
      }
    });
    return itemsToRender;
  };

  const {
    textActiveColor,
    textInactiveColor,
    placeholder,
    value,
    onChange,
    onSubmitEditing,
    isPickerActive,
    handleChange,
    pickerItems,
    hasLabel,
    label,
    pickerWithLableStyle,
  } = props;
  const textStyles: PropStylesInterface = {
    activePickerText: {
      fontWeight: '600',
      color: textActiveColor,
      width: '100%',
    },
    inactivePickerText: {
      fontWeight: '600',
      color: textInactiveColor,
      width: '100%',
    },
    activePickerPlaceholder: {
      color: textActiveColor,
    },
    inactivePickerPlaceholder: {
      color: textInactiveColor,
    },
  };
  return (
    <View
      style={
        hasLabel ? [pickerWithLableStyle, styles.inputWithLableStyle] : null
      }
    >
      {hasLabel && (
        <View style={styles.labelContainerStyle}>
          <Text>{label}</Text>
        </View>
      )}
      <View>
        <Picker
          placeholder={placeholder}
          mode="dropdown"
          selectedValue={value}
          onValueChange={(pickerValue: any) => {
            const fakeEvent: FakeEvent = {
              target: {
                value: pickerValue,
              },
            };
            handleChange(fakeEvent);
            if (onChange) {
              onChange(pickerValue);
            }
            if (pickerValue && onSubmitEditing) {
              onSubmitEditing();
            }
          }}
          placeholderStyle={
            isPickerActive
              ? textStyles.activePickerPlaceholder
              : textStyles.inactivePickerPlaceholder
          }
          textStyle={
            isPickerActive
              ? textStyles.activePickerText
              : textStyles.inactivePickerText
          }
          enabled
        >
          {renderItems(pickerItems)}
        </Picker>
      </View>
    </View>
  );
};

export default PickerComponent;
