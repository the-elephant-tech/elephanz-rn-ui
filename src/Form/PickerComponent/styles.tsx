import {
  ImageStyle, StyleSheet, TextStyle, ViewStyle,
} from 'react-native';


interface Styles {
  dropdownArrowContainer: ViewStyle;
  dropdownImage: ImageStyle;
  errorContainer: ViewStyle;
  inputError: TextStyle;
  pickerContainerActiveStyle: ViewStyle;
  pickerContainerInactiveStyle: ViewStyle;
  pickerWithLableStyle?: ViewStyle;
  inputWithLableStyle: ViewStyle;
  labelContainerStyle: ViewStyle;
}

export const styles = StyleSheet.create<Styles>({
  dropdownArrowContainer: {
    position: 'absolute',
    right: 0,
    width: 24,
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  dropdownImage: {
    resizeMode: 'contain',
    width: 16,
    height: 16,
  },
  errorContainer: {
    paddingHorizontal: 16,
    height: 12,
  },
  inputError: {
    fontSize: 12,
  },
  pickerContainerActiveStyle: {
    flex: 1,
    flexDirection: 'row',
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
  },
  pickerContainerInactiveStyle: {
    flex: 1,
    flexDirection: 'row',
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
  },
  inputWithLableStyle: {
    marginVertical: 5,
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  labelContainerStyle: {
    width: '30%',
    paddingLeft: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  pickerWithLableStyle: {
    display: 'flex',
    flexDirection: 'row',
  },
});

export default styles;
