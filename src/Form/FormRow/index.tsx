import * as React from 'react';
import {
  View,
} from 'react-native';
import styles from './styles';
import {
  FormRowProps,
} from './types';

export const FormRow: React.FC<
FormRowProps
> = (props) => {
  const {
    style, children,
  } = props;
  return <View style={style || styles.pickerContainer}>{children}</View>;
};

export default FormRow;
