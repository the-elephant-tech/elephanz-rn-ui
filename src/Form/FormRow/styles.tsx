import {
  StyleSheet, ViewStyle,
} from 'react-native';

interface Styles {
  pickerContainer: ViewStyle;
}

export const styles = StyleSheet.create<Styles>({
  pickerContainer: {
    flexDirection: 'row',
    marginVertical: 5,
  },
});

export default styles;
