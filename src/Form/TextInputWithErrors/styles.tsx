import {
  StyleSheet, TextStyle, ViewStyle,
} from 'react-native';

interface Styles {
  textInputContainer: ViewStyle;
  inputFieldStyleCorrect: TextStyle;
  inputFieldStyleIncorrect: TextStyle;
  errorContainer: ViewStyle;
  inputError: TextStyle;
  inputWithLableStyle: ViewStyle;
  labelContainerStyle: TextStyle;
}

export const styles = StyleSheet.create<Styles>({
  textInputContainer: {
    marginVertical: 5,
  },
  inputFieldStyleCorrect: {
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    paddingHorizontal: 16,
  },
  inputFieldStyleIncorrect: {
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    paddingHorizontal: 16,
  },
  errorContainer: {
    paddingHorizontal: 16,
    height: 12,
  },
  inputError: {
    fontSize: 12,
  },
  inputWithLableStyle: {
    marginVertical: 5,
    flexDirection: 'row',
    // width: '70%',
    alignContent: 'stretch',
  },
  labelContainerStyle: {
    width: '30%',
    alignSelf: 'center',
    paddingLeft: 20,
  },
});

export default styles;
