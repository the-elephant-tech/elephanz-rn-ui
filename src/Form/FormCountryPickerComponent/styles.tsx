import {
  StyleSheet, TextStyle, ViewStyle,
} from 'react-native';

interface Styles {
  correctPickerStyle: ViewStyle;
  incorrectPickerStyle: ViewStyle;
  inputError: TextStyle;
  inputWithLableStyle: ViewStyle;
  textInputContainer: ViewStyle;
  labelContainerStyle: TextStyle;
  countryPickerContainer: ViewStyle;
  callingCode: TextStyle;
}

export const styles = StyleSheet.create<Styles>({
  correctPickerStyle: {
    flexDirection: 'row',
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  incorrectPickerStyle: {
    flexDirection: 'row',
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputError: {
    fontSize: 12,
    marginLeft: 20,
  },
  inputWithLableStyle: {
    marginVertical: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'stretch',
  },
  textInputContainer: {
    marginVertical: 5,
  },
  labelContainerStyle: {
    width: '30%',
    alignSelf: 'center',
    paddingLeft: 20,
  },
  countryPickerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  callingCode: {
    fontSize: 14,
    marginHorizontal: 10,
  },
});

export default styles;
