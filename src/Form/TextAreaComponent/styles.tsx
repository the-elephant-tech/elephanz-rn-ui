import {
  StyleSheet, TextStyle, ViewStyle,
} from 'react-native';

interface Styles {
  textInputContainer: ViewStyle;
  inputFieldStyleCorrect: TextStyle;
  inputFieldStyleIncorrect: TextStyle;
  errorContainer: ViewStyle;
  inputError: TextStyle;
  inputWithLableStyle: ViewStyle;
  labelContainerStyle: TextStyle;
}

export const styles = StyleSheet.create<Styles>({
  textInputContainer: {
    marginVertical: 5,
  },
  inputFieldStyleCorrect: {
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderBottomWidth: 2,
    borderRadius: 20,
    padding: 16,
  },
  inputFieldStyleIncorrect: {
    borderTopWidth: 2,
    borderRightWidth: 2,
    borderLeftWidth: 2,
    borderBottomWidth: 2,
    borderRadius: 20,
    padding: 16,
  },
  errorContainer: {
    paddingHorizontal: 16,
    height: 12,
  },
  inputError: {
    fontSize: 12,
  },
  inputWithLableStyle: {
    marginVertical: 5,
    flexDirection: 'column',
    alignContent: 'stretch',
  },
  labelContainerStyle: {
    width: '30%',
    alignSelf: 'flex-start',
    paddingLeft: 20,
  },
});

export default styles;
