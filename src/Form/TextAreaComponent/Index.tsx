import {
  FormikErrors, FormikTouched,
} from 'formik';
import {
  Textarea,
} from 'native-base';
import React, {
  createRef,
} from 'react';
import {
  Text, TextStyle, View,
} from 'react-native';
import Theme from '../../Theme/DefaultTheme';
import styles from './styles';
import {
  TextAreaComponentProps,
} from './types';

export const TextAreaComponent: React.FC<
TextAreaComponentProps
> = (props) => {
  const {
    colors,
  } = Theme;

  const inputRef = createRef<Textarea>();
  // const focus = () => {
  //   if (inputRef) {
  //     (inputRef.current as any)._root.focus();
  //   }
  // };

  // const blur = () => {
  //   if (inputRef) {
  //     (inputRef.current as any)._root.blur();
  //   }
  // };

  const selectInputStyle = (
    error: string | string[] | undefined | FormikErrors<any> | FormikErrors<any>[],
    touched: boolean | FormikTouched<any> | FormikTouched<any>[] | undefined,
  ): TextStyle => {
    const {
      incorrectFieldStyle, correctFieldStyle,
    } = props;
    if (error && touched) {
      if (incorrectFieldStyle) {
        return incorrectFieldStyle;
      }
      return styles.inputFieldStyleIncorrect;
    }
    if (correctFieldStyle) {
      return correctFieldStyle;
    }
    return styles.inputFieldStyleCorrect;
  };

  const {
    value,
    onChange,
    onBlur,
    placeholder,
    touched,
    error,
    errorsStyle,
    secureTextEntry,
    onSubmitEditing,
    returnKeyType,
    selectTextOnFocus,
    clearTextOnFocus,
    onFocus,
    hasLabel,
    label,
    rowSpan,
    bordered,
    underline,
    handleChange,
  } = props;
  return (
    <View
      style={hasLabel ? styles.inputWithLableStyle : styles.textInputContainer}
    >
      {hasLabel && <Text style={styles.labelContainerStyle}>{label}</Text>}
      <Textarea
        rowSpan={rowSpan}
        bordered={bordered}
        underline={underline}
        ref={inputRef}
        style={
          hasLabel
            ? [styles.inputWithLableStyle, selectInputStyle(error, touched)]
            : [selectInputStyle(error, touched)]
        }
        value={value}
        onChangeText={(e) => {
          handleChange(e);
          if (onChange) {
            onChange(e);
          }
        }}
        onBlur={onBlur}
        placeholder={placeholder}
        placeholderTextColor={colors.secondaryTextColor}
        secureTextEntry={secureTextEntry}
        onSubmitEditing={onSubmitEditing}
        returnKeyType={returnKeyType}
        selectTextOnFocus={selectTextOnFocus}
        clearTextOnFocus={clearTextOnFocus}
        onFocus={onFocus}
      />
      <View style={styles.errorContainer}>
        {touched && error && (
          <Text style={errorsStyle || styles.inputError}>{error}</Text>
        )}
      </View>
    </View>
  );
};

export default TextAreaComponent;
