import {
  StyleSheet, ViewStyle,
} from 'react-native';

interface Styles {
  rowItem: ViewStyle;
  lastRowItem: ViewStyle;
}

export const styles = StyleSheet.create<Styles>({
  rowItem: {
    flex: 1,
    marginRight: 16,
  },
  lastRowItem: {
    marginRight: 0,
  },
});

export default styles;
