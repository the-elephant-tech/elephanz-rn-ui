import {
  StyleSheet, TextStyle,
} from 'react-native';

interface StylesInterface {
  forgetPassword: TextStyle;
}

export const styles = StyleSheet.create<StylesInterface>({
  forgetPassword: {
    justifyContent: 'center',
    alignSelf: 'center',
    textDecorationLine: 'underline',
    marginTop: 5,
  },
});

export default styles;
