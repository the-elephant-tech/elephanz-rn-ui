import React from 'react';
import {
  Text, TouchableHighlight,
} from 'react-native';
import Theme from '../../Theme/DefaultTheme';
import {
  styles,
} from './styles';
import {
  Props,
} from './types';

export const ClickableText: React.FC<
Props
> = (props) => {
  const {
    style, onPress, text, disabled,
  } = props;
  const {
    colors,
  } = Theme;
  return (
    <TouchableHighlight disabled={disabled}>
      <Text
        style={
          style || [styles.forgetPassword, {
            color: colors.secondaryColor,
          }]
        }
        onPress={onPress}
      >
        {text}
      </Text>
    </TouchableHighlight>
  );
};

export default ClickableText;
