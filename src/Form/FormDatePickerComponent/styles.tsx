import {
  ImageStyle, StyleSheet, TextStyle, ViewStyle,
} from 'react-native';

interface Styles {
  correctPickerStyle: ViewStyle;
  incorrectPickerStyle: ViewStyle;
  pickerImage: ImageStyle;
  pickerText: TextStyle;
  pickerActiveText: TextStyle;
  inputError: TextStyle;
  inputWithLableStyle: ViewStyle;
  labelContainerStyle: ViewStyle;
  pickerContainerStyle: ViewStyle;
  dateContainer: ViewStyle;
}

export const styles = StyleSheet.create<Styles>({
  correctPickerStyle: {
    flexDirection: 'row',
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  incorrectPickerStyle: {
    flexDirection: 'row',
    height: 45,
    borderWidth: 2,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  pickerImage: {
    marginLeft: 55,
    width: 28,
    height: 28,
  },
  pickerText: {
    fontSize: 14,
    fontFamily: 'SF Pro Display',
    justifyContent: 'flex-end',
  },
  pickerActiveText: {
    fontSize: 14,
    fontFamily: 'SF Pro Display',
    justifyContent: 'flex-end',
  },
  inputError: {
    fontSize: 12,
    marginLeft: 20,
  },
  inputWithLableStyle: {
    marginVertical: 5,
    flexDirection: 'row',
    alignContent: 'stretch',
  },
  labelContainerStyle: {
    width: '30%',
    alignSelf: 'center',
    paddingLeft: 20,
  },
  pickerContainerStyle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  dateContainer: {
    display: 'none',
  },
});

export default styles;
