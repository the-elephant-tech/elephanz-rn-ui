import React from 'react';
import {
  Text, TouchableOpacity, View,
} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {
  FakeEvent,
} from '../types';
import styles from './styles';
import {
  DatePickerProps,
} from './types';

export const FormDatePickerComponent: React.FC<
DatePickerProps
> = (props) => {
  let datePickerRef: DatePicker | null;
  const focus = () => {
    if (datePickerRef) {
      datePickerRef.onPressDate();
    }
  };

  // const blur = () => {
  //   if (datePickerRef) {
  //     datePickerRef.onPressCancel();
  //   }
  // };

  const selectContainerStyle = () => {
    const {
      hasLabel, inputWithLableStyle,
    } = props;
    if (hasLabel) {
      if (inputWithLableStyle) {
        return inputWithLableStyle;
      }
      return styles.inputWithLableStyle;
    }
    return null;
  };

  const {
    onPress,
    placeholder,
    touched,
    error,
    value,
    onChange: onDateChange,
    format,
    minDate,
    maxDate,
    confirmBtnText,
    cancelBtnText,
    mode,
    onSubmitEditing,
    hasLabel,
    label,
    handleChange,
  } = props;
  return (
    <View style={selectContainerStyle()}>
      {hasLabel && <Text style={styles.labelContainerStyle}>{label}</Text>}
      <View style={styles.pickerContainerStyle}>
        <TouchableOpacity
          style={
            touched && error
              ? styles.incorrectPickerStyle
              : styles.correctPickerStyle
          }
          onPress={() => {
            // datePickerComponentRef.onPress();
            // datePickerComponentRef.focus();
            // onPressDate();
            focus();
            onPress();
          }}
        >
          <Text>
            {typeof value === 'string' ? value : value.toLocaleDateString()}
          </Text>
        </TouchableOpacity>
        {touched && error && value === 'Birth Date' && (
          <Text style={styles.inputError}>{error}</Text>
        )}
      </View>
      <View
        style={styles.dateContainer}
      >
        <DatePicker
          ref={(ref) => datePickerRef = ref}
          date={typeof value === 'string' ? new Date() : value}
          mode={mode}
          placeholder={placeholder}
          format={format}
          minDate={minDate}
          maxDate={maxDate}
          confirmBtnText={confirmBtnText}
          cancelBtnText={cancelBtnText}
          onDateChange={(dateStr, date) => {
            const d: FakeEvent = {
              target: {
                value: date,
              },
            };
            handleChange(d);
            if (onDateChange) {
              onDateChange(date);
            }

            if (onSubmitEditing && date) {
              onSubmitEditing();
            }
          }}
        />
      </View>

    </View>
  );
};

export default FormDatePickerComponent;
