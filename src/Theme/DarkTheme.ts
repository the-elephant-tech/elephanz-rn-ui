import {
  ThemeColors,
} from './types';

export class DarkTheme implements ThemeColors {
  primaryColor ='#FFFFF';

  secondaryColor ='#4BD2D2';

  primaryGradient =['#82EBEB', '#4BD2D2'];

  secondaryGradient =['#808080', '#4BD2D2'];

  primaryDisabledColor ='#9e9e9e';

  secondaryDisabledColor ='#e0e0e0';

  primaryTextColor ='#00000';

  secondaryTextColor ='#fffff';

  linkColor ='#4a90e2';

  borderShadow ='#00000';
}

export default new DarkTheme();
