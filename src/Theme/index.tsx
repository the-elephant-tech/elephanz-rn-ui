import DarkTheme from './DarkTheme';
import DefaultTheme from './DefaultTheme';

class Themes {
  static DefaultTheme = DefaultTheme;

  static DarkTheme = DarkTheme;
}

export default Themes;
