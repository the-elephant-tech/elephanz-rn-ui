
export interface ThemeColors {
  primaryColor: string;
  primaryGradient: string[];
  secondaryGradient: string[];
  primaryTextColor: string;
  secondaryTextColor: string;
  linkColor: string;
  primaryDisabledColor: string;
  secondaryDisabledColor: string;
  borderShadow: string;
}

export interface ThemeContext {
  theme: ThemeColors;
  setTheme: (theme: ThemeColors) => void;
}
