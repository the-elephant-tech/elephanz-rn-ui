import {
  ThemeColors,
} from './types';

export class DefaultTheme implements ThemeColors {
  primaryColor = '#0F4265';

  secondaryColor = '#4BD2D2';

  primaryGradient = ['#82EBEB', this.secondaryColor];

  secondaryGradient = [this.primaryColor, this.secondaryColor];

  primaryDisabledColor = '#9e9e9e';

  secondaryDisabledColor = '#e0e0e0';

  primaryTextColor = '#00000';

  secondaryTextColor = '#fffff';

  linkColor = '#4a90e2';

  borderShadow = '#00000';
}

export default new DefaultTheme();
