import React from 'react';
import Themes from './index';
import {
  ThemeContext as ThemeContextType,
} from './types';

const ThemeContext = React.createContext<ThemeContextType>({
  theme: Themes.DefaultTheme,
  setTheme: () => null,
});

export default ThemeContext;
