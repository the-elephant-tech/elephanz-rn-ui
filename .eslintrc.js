module.exports = {
    extends: [
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:@typescript-eslint/recommended',
        'airbnb-typescript',
    ],
    plugins: ['react', '@typescript-eslint'],
    env: {
        browser: true,
        es6: true,
        jasmine: true,
        jest: true,
        node: true,
    },
    rules: {
        '@typescript-eslint/explicit-member-accessibility': 0,
        '@typescript-eslint/explicit-function-return-type': 0,
        'object-property-newline': ['error'],
        "react/prop-types": 0,
        'object-curly-newline': ['error', 'always'],
        '@typescript-eslint/no-unused-vars': ['error', { 'ignoreRestSiblings': true }],
        'quotes': ['error', 'single'],
        'no-return-assign': 0,
        'global-require': 0,
        'max-len': ['error', 120],
        'import/no-named-as-default': 0,
        'class-methods-use-this': 0,
        'no-param-reassign': 0,
        'react/jsx-props-no-spreading': 0,
        'import/no-unresolved': ['error', {
            caseSensitive: false,
        }],
    },
    settings: {
        react: {
            pragma: 'React',
            version: 'detect',
        },
        "import/resolver": {
            "typescript": {},
        }
    },
    parser: '@typescript-eslint/parser',
};
